from setuptools import setup

setup(
    name='porridge',
    version='0.0.0',
    author='Fritz Mahnke',
    author_email='fritz@fritzmahnke.com',
    description='Mass color measurement analysis',
    classifiers=[
        'Programming Language :: Python :: 2.7',
    ],
    keywords='color_management color',
    packages=['porridge'],
    install_requires=['openpyxl<2.0.0', 'pandas'],
    tests_require=['pytest']
)
