import pandas as pd

def get(path):
    ''' Get a pd ExcelWriter. '''
    return pd.ExcelWriter(path)

def addSheet(xls, pd_dataframe, sheetname):
    pd_dataframe.to_excel(xls, sheetname)
    xls.save()

def addSheets(xls, pd_dataframes, sheetnames=None):
    if sheetnames is None:
        sheetnames = [str(x) for x in range(len(pd_dataframes))]

    assert len(sheetnames) == len(pd_dataframes)

    index = 0

    while index < len(pd_dataframes):
        pd_dataframes[index].to_excel(xls, sheetnames[index])
        index += 1

    xls.save()
