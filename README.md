# porridge - Color analyses with Python

porridge makes it possible to do large-scale analysis of color measurement data. It includes a CGATS
file parser and the ability to combine many color measurements into groups for processing. It is
built on pandas.

## Installation

It is recommended to install porridge in a virtualenv. For more information on Python virtualenvs,
see (http://docs.python-guide.org/en/latest/dev/virtualenvs/).

To install the package locally with pip:

`
git clone <repo-url>
pip install -e .
`

## Tests

To run tests, execute `py.test`

To do:

* Reporting module reads device list and track list from file or command line arguments.

Ideas for features/use cases to cover:

* Average color data in 2+ reference files with the same patch set.
* When averaging, discard patches from influencing the average if they deviate too much from the rest of the set.
* Calculate dE statistics (per patch, average, standard deviation) between 2+ reference files with the same patch set.
* Sort reference files on one or more columns.
* Print verification of sample vs. reference based on tolerances.
* Generate CGATS reference files for measurement devices based on tab-delimited color patch data.
* Look at curves of the pure primary channels (C, M, Y, K) and (?) secondaries (R, G, B).
* Analyze a "uniformity chart". The chart may contain several duplicate patch groups designed to evaluate uniformity. Technically, we'd need to isolate all "duplicate" patches on the chart, create "groups" from them, and compare the groups.
* Allow "custom fields" to be added per set of data. So the user could add a "device name" to distinguish data sets for different devices.
* Allow to add a note to a tab-delimited (CGATS) file, or batch add the same note to a group of tab-delimited files.
