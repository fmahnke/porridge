import porridge.cgats as cg
import porridge.dataset_ops as dataset_ops
import porridge.dataset as dataset

import os
import unittest

import numpy as np

TEST_PATH = 'test'
TEST_DATA_PATH = TEST_PATH + os.sep + 'test_data'
MB_SAMPLE_SIZE = 27

class TestDataSetOps(unittest.TestCase):
    def test_std_dev_by_rgb(self):
        names = [TEST_DATA_PATH + os.sep + '8_6_2013 4_44 PM.txt',
                 TEST_DATA_PATH + os.sep + '8_6_2013 4_45 PM.txt',
                 TEST_DATA_PATH + os.sep + '8_6_2013 4_46 PM.txt']

        ds = dataset.from_filenames(names)
        std_dev_lab = dataset_ops.std_dev_by_rgb(ds.data)

        first_five = {cg.L : [0.589835, 0.154477, 0.235311, 0.372720, 0.265566],
                      cg.A : [0.051718, 0.453629, 0.586438, 0.144242, 0.632933],
                      cg.B : [0.091542, 0.294917, 0.227958, 0.550507, 0.264460]}

        last_five = {cg.L : [0.162489, 0.183660, 0.120644, 0.212966, 0.118145],
                     cg.A : [0.282852, 0.085522, 0.139923, 0.235028, 0.009292],
                     cg.B : [0.129620, 0.323005, 0.344666, 0.297334, 0.030713]}

        self.assertEqual(MB_SAMPLE_SIZE, len(std_dev_lab))

        rounded = std_dev_lab[[cg.L, cg.A, cg.B]].apply(np.round, decimals=6)
        self.assertTrue(first_five, rounded[:5])
        self.assertTrue(last_five, rounded[-5:])
