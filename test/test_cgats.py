import porridge.cgats as cg

import os
import unittest
import datetime as dt

TEST_PATH = 'test'
TEST_DATA_PATH = TEST_PATH + os.sep + 'test_data'
TEST_DATA_ISO_01 = 'ISO Strip 5 Before.txt'
TEST_DATA_ISO_02 = 'ISO Strip 5 After.txt'
TEST_DATA_GRACOL = 'GRACoL2006_Coated1.txt'
ISO_SAMPLE_SIZE = 54

CGATS_TEST_1 = 'colorport_cmyk_lab_spectral_cgats.txt'

class TestCgats(unittest.TestCase):
    def test_cgats_from_file(self):
        cgats = cg.from_file(TEST_DATA_PATH + os.sep + CGATS_TEST_1)
        self.assertEqual(ISO_SAMPLE_SIZE, len(cgats.data))

    def test_cgats_from_files(self):
        filenames = [TEST_DATA_PATH + os.sep + TEST_DATA_ISO_01,
                     TEST_DATA_PATH + os.sep + TEST_DATA_ISO_02]

        cgats = cg.from_files(filenames)
        self.assertEqual(len(filenames), len(cgats))

    def test_header_tab_separated(self):
        ''' Test header dictionary is correct when values are separated from keys by tabs. '''
        cgats = cg.from_file(TEST_DATA_PATH + os.sep + TEST_DATA_ISO_01)
        self.assertEqual('40', cgats.header['NUMBER_OF_FIELDS'])

    def test_header_space_separated(self):
        ''' Test header dictionary is correct when values are separated from keys by spaces. '''
        cgats = cg.from_file(TEST_DATA_PATH + os.sep + TEST_DATA_GRACOL)
        self.assertEqual('33', cgats.header['LGOROWLENGTH'])

    def test_parse_timestamp(self):
        timestamps = ['8/6/2013 11:16:41 AM', '2013/08/06', '"2013/08/06"']
        year, month, day = cg.parse_timestamp(timestamps[0])
        time1 = dt.datetime(year, month, day)
        self.assertEqual(2013, time1.year)
        self.assertEqual(8, time1.month)
        self.assertEqual(6, time1.day)
        year, month, day = cg.parse_timestamp(timestamps[1])
        time2 = dt.datetime(year, month, day)
        self.assertEqual(2013, time2.year)
        self.assertEqual(8, time2.month)
        self.assertEqual(6, time2.day)
        year, month, day = cg.parse_timestamp(timestamps[2])
        time3 = dt.datetime(year, month, day)
        self.assertEqual(2013, time3.year)
        self.assertEqual(8, time3.month)
        self.assertEqual(6, time3.day)
