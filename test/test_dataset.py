import porridge.cgats as cg
import porridge.dataset as dataset

import fnmatch
import os
import unittest

import numpy as np

TEST_PATH = 'test'
TEST_DATA_PATH = TEST_PATH + os.sep + 'test_data'
TEST_DATA_ISO_01 = 'ISO Strip 5 Before.txt'
TEST_DATA_ISO_02 = 'ISO Strip 5 After.txt'
TEST_DATA_MACBETHS = '8_6_2013?4_4*'
ISO_SAMPLE_SIZE = 54
MB_SAMPLE_SIZE = 27

class TestDataSet(unittest.TestCase):
    def setUp(self):
        self.colorDataSet1 = dataset.DataSet()
        self.colorDataSet2 = dataset.DataSet()

    def test_load_cgats_as_group(self):
        ds = dataset.from_file_pattern(TEST_DATA_PATH + os.sep + '8_6_2013?4_4*',
            group = 'test_group')
        self.assertEqual(1, len(ds.groups))
        self.assertEqual(9, len(ds.groups['test_group']))

    def test_add_group(self):
        ds = dataset.from_file_pattern(TEST_DATA_PATH + os.sep + '8_6_2013?4_4*')
        ds.add_group('group1', range(5))
        self.assertEqual(1, len(ds.groups))
        self.assertEqual(5, len(ds.groups['group1']))
        ds.add_group('group2', range(5, 9))
        self.assertEqual(2, len(ds.groups))
        self.assertEqual(4, len(ds.groups['group2']))

    def test_add_filepattern_to_dataset(self):
        ds = dataset.from_file_pattern(TEST_DATA_PATH + os.sep + '8_6_2013?4_4*', 'group1')

        self.assertEqual(MB_SAMPLE_SIZE, len(ds.get_set(8)))
        self.assertEqual(MB_SAMPLE_SIZE * 9, len(ds.data))

        self.assertTrue(ds.groups['group1'] == range(9))

        dataset.add_file_pattern(ds, TEST_DATA_PATH + os.sep + '8_6_2013?4_4*', 'group2')

        self.assertEqual(MB_SAMPLE_SIZE, len(ds.get_set(12)))
        self.assertEqual(MB_SAMPLE_SIZE * 9 * 2, len(ds.data))

        self.assertTrue(ds.groups['group2'] == range(9, 18))

    def test_group(self):
        ds = dataset.from_file_pattern(TEST_DATA_PATH + os.sep + '8_6_2013?4_4*', 'group1')
        dataset.add_file_pattern(ds, TEST_DATA_PATH + os.sep + '8_6_2013?4_4*', 'group2')

        group1 = ds.group('group1')
        group2 = ds.group('group2')

        self.assertEqual(MB_SAMPLE_SIZE * 9, len(group1))
        self.assertEqual(MB_SAMPLE_SIZE * 9, len(group2))

    def test_get_samples_when_none_exist(self):
        self.assertIsNone(self.colorDataSet1.data)
        self.assertIsNone(self.colorDataSet2.data)

    def test_dataset_from_file_pattern(self):
        ds = dataset.from_file_pattern(TEST_DATA_PATH + os.sep + '8_6_2013?4_4*')

        self.assertEqual(MB_SAMPLE_SIZE, len(ds.get_set(8)))
        self.assertEqual(MB_SAMPLE_SIZE * 9, len(ds.data))

    def test_get_samples_for_MB(self):
        names = []

        for file in os.listdir(TEST_DATA_PATH):
            if fnmatch.fnmatch(file, TEST_DATA_MACBETHS):
                names.append(TEST_DATA_PATH + os.sep + file)

        NUMBER_OF_SETS = len(names)

        ds = dataset.from_filenames(names)
        self.assertEqual(MB_SAMPLE_SIZE, len(ds.get_set(NUMBER_OF_SETS - 1)))
        self.assertEqual(MB_SAMPLE_SIZE * NUMBER_OF_SETS, len(ds.data))

    def test_get_samples_for_ISO_01(self):
        self.colorDataSet1 = dataset.from_filenames([TEST_DATA_PATH + os.sep + TEST_DATA_ISO_01])
        self.assertEqual(ISO_SAMPLE_SIZE, len(self.colorDataSet1.get_set(0)))

    def test_get_samples_for_ISO_02(self):
        self.colorDataSet2 = dataset.from_filenames([TEST_DATA_PATH + os.sep + TEST_DATA_ISO_02])
        self.assertEqual(ISO_SAMPLE_SIZE, len(self.colorDataSet2.get_set(0)))
