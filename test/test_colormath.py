import porridge.calc
import porridge.cgats as cg
import porridge.dataset as dataset

import numpy as np
import os
import unittest

TEST_PATH = 'test'
TEST_DATA_PATH = TEST_PATH + os.sep + 'test_data'
TEST_DATA_ISO_01 = 'ISO Strip 5 Before.txt'
TEST_DATA_ISO_02 = 'ISO Strip 5 After.txt'
TEST_DATA_GRACOL = 'GRACoL2006_Coated1.txt'
ISO_SAMPLE_SIZE = 54

class TestColorMath(unittest.TestCase):
    def test_color_differences(self):
        self.colorDataSet1 = dataset.from_filenames([TEST_DATA_PATH + os.sep + TEST_DATA_ISO_01])
        self.colorDataSet2 = dataset.from_filenames([TEST_DATA_PATH + os.sep + TEST_DATA_ISO_02])

        diffs, stats = porridge.calc.dE76_datasets(self.colorDataSet1, self.colorDataSet2)
        self.assertEqual(ISO_SAMPLE_SIZE, len(diffs))

    def test_color_differences_subset(self):
        self.colorDataSet1 = dataset.from_filenames([TEST_DATA_PATH + os.sep + TEST_DATA_ISO_01])
        self.colorDataSet2 = dataset.from_filenames([TEST_DATA_PATH + os.sep + TEST_DATA_GRACOL])
        diffs, stats = porridge.calc.dE76_datasets(self.colorDataSet1, self.colorDataSet2)

    def test_vector_magnitude(self):
        vectors = [[-1.04, 1.45, 0.04], [-1.75, 2.29, 0.49],
                   [-4.07, 3.18, -1.10]]
        expected_magnitudes = [1.78, 2.92, 5.28]
                                      
        magnitudes = [porridge.calc.magnitude(x) for x in vectors]

        self.assertEqual(len(expected_magnitudes), len(magnitudes))

        for i in range(len(magnitudes)):
            self.assertAlmostEqual(expected_magnitudes[i], magnitudes[i], 2)

    def test_mean(self):
        names = [TEST_DATA_PATH + os.sep + '8_6_2013 4_44 PM.txt',
                 TEST_DATA_PATH + os.sep + '8_6_2013 4_45 PM.txt',
                 TEST_DATA_PATH + os.sep + '8_6_2013 4_46 PM.txt']

        ds = dataset.from_filenames(names)
        mean_lab = porridge.calc.mean(ds.data)

        first_five = {cg.L : [23.166627, 49.451147, 50.765997, 40.212617, 35.440113],
                      cg.A : [0.961360, 45.170343, -23.425177, 13.916347, -23.425177],
                      cg.B : [2.398850, 12.986510, -22.648620, 11.573973, -20.139690]}

        last_five = {cg.L : [79.903543, 96.258737, 41.471810, 50.547410, 52.270863],
                     cg.A : [0.915987, -0.701233, 7.888223, 46.434967, 0.988723],
                     cg.B : [70.867797, 3.485113, -35.663340, -10.700637, 0.986960]}

        self.assertEqual(27, len(mean_lab))

        rounded = mean_lab[[cg.L, cg.A, cg.B]].apply(np.round, decimals = 6)
        self.assertTrue(first_five, rounded[:5])
        self.assertTrue(last_five, rounded[-5:])

