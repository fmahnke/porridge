import json
import os
import zipfile

def from_file(path):
    ''' Deserialize a JSON file to a Python object. '''
    if os.path.isfile(path):
        with open(path, 'r') as f:
            data = json.load(f)
        return data

def to_file(data, path):
    with open(path, 'w') as f:
        json.dump(data, f)

def make_containing_directory_from_file_path(cache_file_path):
    ''' Given a path to a file, create its containing directory if nonexistent. '''
    cache_dir = cache_file_path[:cache_file_path.rfind('/')]
    if not os.path.isdir(cache_dir):
        os.makedirs(cache_dir)

def zip_files(zip_filename, files):
    ''' Compress files into a zip file. If the file exists, it is overwritten. '''
    with zipfile.ZipFile(zip_filename, 'w') as zip_file:
        for filename in files:
            filename_only = filename[filename.rfind('/') + 1:]
            zip_file.write(filename, filename_only)
