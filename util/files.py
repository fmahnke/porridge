import os
import fnmatch

def names_from_pattern(pattern):
    ''' Construct a list of filenames on the filesystem from a pattern. '''

    directory = ''
    filenames = []

    filepart_index = pattern.rfind(os.sep)

    if filepart_index != -1:
        directory += pattern[0:filepart_index + 1]
        filepart = pattern[filepart_index + 1:]
    else:
        filepart = pattern

    for file in os.listdir(directory):
        if fnmatch.fnmatch(file, filepart):
            filenames.append(directory + os.sep + file)

    return filenames

