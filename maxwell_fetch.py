import httplib
import os
import sys
import time
import urllib
import urllib2
import zipfile

'''
This module fetches device measurement data from the pymx service connected to the CHROMiX Maxwell
API.
'''

__HTTP_OK = 200
__HTTP_NO_RESPONSE = 204
__HTTP_TIMEOUT_SECONDS = 600

def _build_url(host, dev, track_specifier, start_date, end_date):
    query = '{"device": "' + dev + '", "track": "' + track_specifier + '", "filter": {"startDateTime": "' + start_date + '", "endDateTime": "' + end_date + '"}}'
    query_encoded = urllib.quote(query, '')
    url = host + '/get_colorset?options=' + query_encoded

    return url

def _is_host_healthy(host):
    try:
        response = urllib2.urlopen(host + '/health', timeout=30)
        if response.code == __HTTP_OK:
            return True, ''
        else:
            return False, 'Host health check failed with response code' + ' ' + str(response.code)
    except urllib2.URLError, e:
        return False, e.reason

def _save_file(path, data):
    f = open(path, 'w')
    f.write(data)
    f.close()

def _extract_zip(path, directory):
    z = zipfile.ZipFile(path)
    z.extractall(directory)
    z.close()

def _fetch_measurements(host, devices, track_specifier, start_date, end_date, path):
    devices_with_measurements = []

    for dev in devices:
        url = _build_url(host, dev, track_specifier, start_date, end_date)

        # Download measurements
        try:
            response = urllib2.urlopen(url, timeout=__HTTP_TIMEOUT_SECONDS)
            data = response.read()
        except httplib.BadStatusLine, e:
            print "timeexcept: ", time.time()
            print e
            print e.args
            print e.message
            return

        # TODO: Deal with case when server returns an empty measurement set. The logic below does
        # not work anymore.

        if response.code == __HTTP_OK:
            output_directory = path + os.sep + dev + os.sep + track_specifier
            output_file = output_directory + os.sep + 'content.zip'

            if not os.path.isdir(output_directory):
                os.makedirs(output_directory)

            _save_file(output_file, data)
            _extract_zip(output_file, output_directory)

            os.remove(output_file)

            devices_with_measurements.append(dev)
        elif response.code == __HTTP_NO_RESPONSE:
            print 'No matching measurements for device ' + dev

    return devices_with_measurements


def process(mx_host, track_specifier, devices, start_date, end_date, path):
    hostOk, error = _is_host_healthy(mx_host)

    if not hostOk:
        print 'Could not contact host at' + ' ' + mx_host + '.' + ' ' + str(error)
        sys.exit(1)

    devices_with_measurements = _fetch_measurements(mx_host, devices, track_specifier, start_date,
        end_date, path)

    return devices_with_measurements
