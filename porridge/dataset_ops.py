import cgats

def median_by_sampleid(dataframe):
    df = dataframe.groupby(cgats.SAMPLEID, as_index=False).median()
    df.rename(columns={'lab_l': 'median_l', 'lab_a': 'median_a', 'lab_b': 'median_b'}, inplace=True)
    return df[['sampleid', 'median_l', 'median_a', 'median_b']]

def std_dev_by_rgb(dataframe):
    return dataframe.groupby(cgats.RGB, as_index=False).std()

def std_dev_by_sampleid(dataframe):
    df = dataframe.groupby(cgats.SAMPLEID, as_index=False).std()
    df.rename(columns={'lab_l': 'std_l', 'lab_a': 'std_a', 'lab_b': 'std_b'}, inplace=True)
    return df[['sampleid', 'std_l', 'std_a', 'std_b']]

