import pandas as pd
import util.files as files
import cgats

def from_filenames(filenames, group=None):
    ''' Construct a dataset from a sequence of filenames. '''

    dataset = DataSet()
    cg = cgats.from_files(filenames)
    dataset.from_cgats(cg, group)

    return dataset

def from_file_pattern(pattern, group=None):
    ''' Construct a dataset from one of more files with a Unix filename pattern. '''

    filenames = files.names_from_pattern(pattern)

    return from_filenames(filenames, group)

def add_file_pattern(dataset, pattern, group=None):
    ''' Add to a dataset from a sequence of filenames. '''

    filenames = files.names_from_pattern(pattern)

    cg = cgats.from_files(filenames)
    dataset.add_cgats(cg, group=group)

def _sets(data):
    assert data

    sets = []

    current_set_number = 0
    last_set_number = data.iloc[-1][cgats.SET_NUMBER]

    while (current_set_number <= last_set_number):
        current_set = data[data[cgats.SET_NUMBER] == current_set_number]
        sets.append(current_set)
        current_set_number += 1

    return sets

class DataSet():
    ''' A set of color values and related information. '''

    def __init__(self):
        self._data = None
        self._column_names = []
        self._groups = {}

    def from_cgats(self, cgats, group=None):
        '''
        Create a DataSet from a list of Cgats objects.

        Each element in the list should have the same number of shape of rows.
        Each element in the list should have the same header structure.
        '''

        self._initialize_columns(cgats)
        self.add_cgats(cgats, group)

    def from_group(self, group):
        ''' Create a new DataSet from one of this DataSet\'s groups '''
        ds = DataSet()
        ds._column_names = list(self._column_names)

        return self._data[self._data[cgats.SET_NUMBER].isin(self.groups[name])]

    def add_cgats(self, cgats, group=None):
        '''
        Add a list of Cgats objects to a DataSet.

        Each element in the list should have the same number of shape of rows.
        Each element in the list should have the same header structure.
        '''

        new_group_index = self._number_of_sets()
        new_df = self._get_df_from_cgats(cgats)

        if group:
            new_df['group'] = group

        if type(self._data) != pd.core.frame.DataFrame:
            self._data = new_df
        else:
            other = new_df
            self._data = self._data.append(other)

        if group:
            self.add_group(group, range(new_group_index, new_group_index + len(cgats)))

    @property
    def data(self):
        ''' Get the data. '''
        return self._data

    @property
    def groups(self):
        ''' Get the groups. '''
        return self._groups

    def group(self, name):
        ''' Get the data contained in a group. '''
        return self._data[self._data[cgats.SET_NUMBER].isin(self.groups[name])]

    def add_group(self, name, set_numbers):
        assert(name not in self._groups)

        self._groups[name] = set_numbers

    def get_set(self, set_number):
        ''' Return the set with the specified set number. '''
        return self._data[self._data[cgats.SET_NUMBER] == set_number]

    def _initialize_columns(self, cg):
        self._column_names = cg[0].field_names[:]
        self._column_names.insert(0, cgats.SET_NUMBER)

    def _get_df_from_cgats(self, cgats_list):
        # TODO: Make this function act on one cgats object instead of a list.
        # It will make handling adding the extra columns e.g. setnumber easier.

        set_number = self._number_of_sets()

        df = None

        for i in cgats_list:
            cgats_data = i.data

            # Add the setnumber to the beginning of each row.
            this_df = pd.DataFrame(cgats_data, columns=i.field_names)
            this_df.insert(0, cgats.SET_NUMBER, set_number)
            # Add the cgats header dictionary
            map(lambda key: this_df.insert(0, key, i.header[key]), i.header)

            if df is None:
                df = this_df
            else:
                df = df.append(this_df)

            set_number += 1

        return df

    def _number_of_sets(self):
        if self._data is None:
            return 0
        else:
            return max(self._data[cgats.SET_NUMBER]) + 1

