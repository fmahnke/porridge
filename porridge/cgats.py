import itertools

# Column names we'll use for color reference data
DATE_MEASURE = 'date_measure'
CREATED = 'created'
SET_NUMBER = 'set_number'
SAMPLEID = 'sampleid'
SAMPLENAME = 'samplename'
SAMPLE_NAME = 'sample_name'
L = 'lab_l'
A = 'lab_a'
B = 'lab_b'
CMYK_C = 'cmyk_c'
CMYK_M = 'cmyk_m'
CMYK_Y = 'cmyk_y'
CMYK_K = 'cmyk_k'
RGB_R = 'rgb_r'
RGB_G = 'rgb_g'
RGB_B = 'rgb_b'
DL = 'dL'
DA = 'da'
DB = 'db'
DE = 'dE'
NM = 'nm'

LAB = [L, A, B]
RGB = [RGB_R, RGB_G, RGB_B]
TIMESTAMP = [DATE_MEASURE, CREATED]

_INTEGER_FIELD_NAMES = [SAMPLEID, SET_NUMBER]

__TAB = "\t"
__SPACE = " "

def from_files(filenames):
    ''' Create a list of CGATS objects from a sequence of files. '''

    return [from_file(filename) for filename in filenames]

def from_file(filename):
    ''' Create a CGATS object from a file. '''
    cgats_string = open(filename).readlines()
    return from_string(cgats_string)

def _is_integer(field_name):
    return field_name in _INTEGER_FIELD_NAMES

def _is_decimal(field_name):
    field_name_lower = field_name.lower()

    return field_name_lower.startswith(NM) or field_name_lower in LAB

def _field_type(field_name):
    if _is_integer(field_name):
        return int
    elif _is_decimal(field_name):
        return float
    else:
        return str

def _convert_field_types(field_types, values):
    assert len(field_types) == len(values)

    row = []

    for t, v in itertools.izip(field_types, values):
        row.append(t(v))

    return row

def from_string(cgats_string):
    ''' Create a CGATS object from CGATS string. '''
    data = []
    field_names = []
    header = {}

    is_in_data_format = False
    is_in_data = False

    for line in cgats_string:
        line = line.strip()

        if len(line) == 0:
            continue

        if line.split()[0] == 'END_DATA_FORMAT':
            is_in_data_format = False
        if is_in_data_format:
            if field_names == []:
                field_names = line.split()
                field_names = [name.lower() for name in field_names]
                field_types = map(_field_type, field_names)

        if line.split()[0] == 'BEGIN_DATA_FORMAT':
            is_in_data_format = True

        if line.split()[0] == 'END_DATA':
            is_in_data = False
        if is_in_data:
            row = line.split()
            row = _convert_field_types(field_types, row)

            assert len(field_names) == len(row)
            data.append(row)
        if line.split()[0] == 'BEGIN_DATA':
            is_in_data = True

        if not is_in_data_format and not is_in_data:
            # In header

            # Try to find a tab separating a key/value pair.
            sep = line.find(__TAB)

            if sep == -1:
                # Couldn't find a tab separating a key/value pair, so try a space.
                sep = line.find(__SPACE)

            if sep != -1:
                key = line[0:sep]
                value = line[sep + 1:].strip()

                header[key] = value

    return Cgats(data, field_names, header)

def parse_timestamp(timestamp):
    timestamp = timestamp.strip('\'" ')
    parts = timestamp.split()
    for part in parts:
        date_candidate = part.split('/')
        if len(date_candidate) == 3:
            if int(date_candidate[0]) > 12:
                year = int(date_candidate[0])
                month = int(date_candidate[1])
                day = int(date_candidate[2])
            else:
                year = int(date_candidate[2])
                day = int(date_candidate[1])
                month = int(date_candidate[0])

    return year, month, day

class Cgats():
    ''' A CGATS color data object. '''

    def __init__(self, data, field_names, header):
        self._data = data
        self._field_names = field_names
        self._header = header

    @property
    def data(self):
        ''' Get the DATA section. '''
        return self._data

    @property
    def header(self):
        ''' Return the header dictionary. '''
        return self._header

    @property
    def field_names(self):
        ''' Get the column names in the DATA_FORMAT section. '''
        return self._field_names

