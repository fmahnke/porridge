import cgats as cg
import math

''' Color data operations. '''

def magnitude(vector):
    ''' Calculate the magnitude of a vector. '''
    assert len(vector) == 3

    return math.sqrt(vector[0] ** 2 + vector[1] ** 2 + vector[2] ** 2)

def mean(dataframe):
    ''' Calculate the arithmetic mean of the DataFrame, grouped by sample id. '''
    return dataframe.groupby(cg.SAMPLEID)[cg.LAB].mean()

def dE76(l2, a2, b2, l1, a1, b1):
    dL = l2 - l1
    da = a2 - a1
    db = b2 - b1
    return magnitude([dL, da, db])

def dE76_datasets(dataset1, dataset2):
    ''' Return color differences and statistics between two DataSets. '''

    lab_fields = [cg.L, cg.A, cg.B]
    diffs = dataset2.data[lab_fields] - dataset1.data[lab_fields]
    diffs.columns = [cg.DL, cg.DA, cg.DB]
    dE_list = diffs.apply(magnitude, axis=1)
    diffs.insert(len(diffs.columns), cg.DE, dE_list)
    return diffs, diffs.mean()

